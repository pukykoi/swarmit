#!/usr/bin/env python

"""
This script make a snapshot for all the docker services running in a swarm .
First it dumps all the running databases. When all the databases are dumped, the snapshot of each running service 
of the swarm will occur.
(supported databases are mysql and postgresql).

For the dump of the databases:
We assume that the folder tree for all the services of the swarm is as follows
<stacks root folder>
    |
    - <stack-name>
        |
        - <service-a>
        - <service-b>
        - <...>
        - .env
            |
            - <service-a>.env
So for example, if the stack name is *dendrite*, and the name of the service for the database is *mydb*,
then the program will look for 
`./dendrite/.env/dendrite_mydb.env`
The environment file should contain the user and password needed to dump the database.
eg. MYSQL_USER, MYSQL_PASSWORD.
If those variables are found in the expected .env file, the backup will happen.
"""


import os
import sys
import time
import docker

RESTIC_PASSWORD = os.getenv("RESTIC_PASSWORD")

client = docker.client.from_env()

class DockerService(object):
    volumes_sources= []
    def __init__(self, service):
        self.name = service.name
        self.stack = service.attrs['Spec']['Labels'].get('com.docker.stack.namespace')
        self.image = service.attrs['Spec']['TaskTemplate']['ContainerSpec']['Image']
        self.tasks = service.tasks
        self.remove = service.remove
        self.volumes = {}
        self.tags = []
        volumes = service.tasks()[-1]['Spec']['ContainerSpec'].get("Mounts") or []
        for v in volumes:
            source = v.get('Source')
            if source == None or source in DockerService.volumes_sources:
                continue
            opts = v.get("VolumeOptions") or {}
            label = opts.get('Labels') or {}
            self.tags.append(label.get('backup'))
            self.volumes[v['Source']] = {'bind':v['Target'], 'tag':self.tags[-1]}
            DockerService.volumes_sources.append(source)
        

def read_env_file(service):
    with open("%s/.env/%s.env"%(service.task, service.name), 'r') as f:
        return_dict = {}
        for l in f.read().split("\n"):
            values = l.split("=")
            if values[0] == "":
                continue
            return_dict[values[0]] = "=".join(values[1:])
        return return_dict

def dump_db(service):
    environment = read_env_file(service)
    running_tasks = [t for t in service.tasks() if t['Status']['State'] == "running"]
    if not running_tasks:
        return
    container = client.containers.get(running_tasks[0]['Status']['ContainerStatus']['ContainerID'])

    if "mysql" in service.tags:
        dump_path = [v['bind'] for k,v in service.volumes.items() if v['tag'] == "mysql"][0]
        environment['MYSQL_USER'] = environment.get("MYSQL_USER") or environment.get("MARIADB_USER")
        environment['MYSQL_PASSWORD'] = environment.get("MYSQL_PASSWORD") or environment.get("MARIADB_PASSWORD")
        environment['MYSQL_DATABASE'] = environment.get("MYSQL_DATABASE") or environment.get("MARIADB_DATABASE") 
        script_name = "mysqldump -u $MYSQL_USER -p $MYSQL_PASSWORD $MYSQL_DATABASE > $DBDUMP_PATH/$MYSQL_DATABASE-$(date +%s).sql"
    elif "postgre" in service.tags:
        dump_path = [v['bind'] for k,v in service.volumes.items() if v['tag'] == "postgre"][0]
        script_name = "pg_dump -U$POSTGRES_USER $POSTGRES_DB | gzip -1 > $DBDUMP_PATH/$POSTGRES_DB-$(date +%s).sql.gz"
    else:
        return

    environment['DBDUMP_PATH'] = dump_path + "/dbdump"

    print("Dump DB for service", service.name)
    print(container.exec_run('/bin/bash -c "mkdir -p %s"'%environment['DBDUMP_PATH']))
    print(container.exec_run('/bin/bash -c "%s"'%script_name, environment=environment))

def get_services():
    services = client.services.list()
    db_services = []
    data_services = []
    for s in services:
        service = DockerService(s)
        if [v for k,v in service.volumes.items() if v['tag'] in ["mysql", "postgre"]]:
            db_services.append(service)           
        elif [v for k,v in service.volumes.items() if v['tag'] == "data"]:
            data_services.append(service)
    return db_services, data_services

def make_snapshot(service, destination_folder):
    environment = {\
    "RESTIC_REPOSITORY":"/mnt/restic",
    "RESTIC_PASSWORD":RESTIC_PASSWORD }
    for k, v in service.volumes.items():
        if v['tag'] == None:
            continue
        source_dir = "/data/%s/%s"%(service.name,k)
        cmd =  "restic backup  --host restic --tag %s %s"%(service.image, source_dir)
            
        print(cmd)
        volumes = { destination_folder : { 'bind': environment['RESTIC_REPOSITORY']},
            k : { 'bind' : source_dir } }
        try:
            result = client.containers.run(image="lobaro/restic-backup-docker", stdout=True, stderr=True,\
            name="restic", environment=environment, volumes=volumes,\
                command=cmd)
        except:
            client.containers.get("restic").remove()
            result = client.containers.run(image="lobaro/restic-backup-docker", stdout=True, stderr=True,\
            name="restic", environment=environment, volumes=volumes,\
                command=cmd)

        print("Service snapshot", service.name)
        print(result)
        client.containers.get("restic").remove()


def backup(destination_folder):
    if RESTIC_PASSWORD == None:
        raise ValueError("Env var RESTIC_PASSWORD must be set. Exit")
    db_services, data_services = get_services()
    for s in db_services:
        print("Dump database", s.name)
        dump_db(s)
    for s in data_services+db_services:
        print("Snap service", s.name)
        s.remove()
        make_snapshot(s, destination_folder)
    

if __name__ == "__main__":
    backup(sys.argv[1])
