#!/usr/bin/env bash
swarm_root=/var/lib/docker/swarm
backup_dest=/mnt/ssd/docker
backup_src=$HOME/docker
stacks="stacks"

if [[ -z $RESTIC_PASSWORD ]]; then
    then echo "RESTIC_PASSWORD environment variable must be set."
    exit 1
fi

cwd=$pwd
cd $backup_src/$stacks
mkdir -p $backup_dest/volumes || cd $cwd && exit 1
cd $cwd

echo "Backup services..."
python backup_services.py $backup_dest/volumes || exit 1

echo "Backup swarm files..."
host_date="$(hostname -s)-$(date +%s%z)"
# Backup the folder containing essential files for each stack (config files, compose file etc.)
if [[ -f "$backup_src/$tacks" ]]; then
    mkdir -p $backup_dest/$stacks
    tar -czvf $backup_dest/$stacks/$stacks-backup-$host_date.tar.gz $backup_src/$stacks
fi

# # !!! This stops docker daemon and backups the swarm !!!
echo "Proceeding with the swarm backup..."
systemctl stop docker || exit 1
tar -czvf $backup_dest/swarm/swarm-backup-$host_date.tar.gz $swarm_root
echo "Restarting docker"
systemctl start docker
echo "Restarting services"
cd $backup_src/$stacks && for f in $(ls); do docker stack deploy -c $f $(echo $f | cut -d "." -f1); done
